Router.configure
  layoutTemplate: 'userLayout'

Router.route '/',
  name: 'home'
  template: 'home'

Router.route '/team/list',
  name: 'teamList'
  template: 'teamList'

Router.route '/division/list',
  name: 'divisionsList'
  template: 'divisionsList'

Router.route '/department/list',
  name: 'departmentsList'
  template: 'departmentsList'

Router.route '/volunteer/profile',
  name: 'volunteerForm'
  template: 'addVolunteerForm'

Router.route '/volunteer',
  name: 'volunteerHome'
  template: 'volunteerHome'

Router.route '/volunteer/shifts',
  name: 'volunteerShifts'
  template: 'volunteerShiftsForm'
  data: () ->
    if this.params && this.params.query
      this.params.query

Router.route '/admin/volunteer/form',
  name: 'volunteerFormBuilder'
  template: 'volunteerFormBuilder'
