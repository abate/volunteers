Meteor.startup ->
  console.log "startup #{Volunteers.eventName}"
  allRoles = ['admin','manager','user']
  if Meteor.roles.find({ _id: { $in: allRoles } }).count() < 1
    for role in allRoles
      Roles.createRole role

  defaultUsers = [
    {
      email: 'manager@example.com',
      password: 'apple1'
      roles: ['manager']
    },
    {
      email: 'admin@example.com',
      password: 'apple1'
      roles: ['admin']
    },
    {
      email: 'user@example.com',
      password: 'apple1'
      roles: ['user']
    },
  ]

  _.each defaultUsers, (options) ->
    if !Meteor.users.findOne({ "emails.address" : options.email })
      userId = Accounts.createUser(options)
      Meteor.users.update(userId, {$set: {"emails.0.verified" :true}})
      for role in options.roles
        if role == 'admin'
          Roles.addUsersToRoles(userId, role)
        else
          Roles.addUsersToRoles(userId, role, Volunteers.eventName)

Meteor.startup ->
  # pick one collection to avoid adding new data at each server restart
  if Volunteers.Collections.Division.find().count() == 0
    Factory.define('fakeUser', Meteor.users, {
      'profile':
        'firstName': () -> faker.name.firstName(),
        'lastName': () -> faker.name.lastName(),
        'role': () -> 'user'
      'emails': () ->
        [
          'address': faker.internet.email(),
          'verified': true
        ]
      }
    ).after((user) ->
      Roles.addUsersToRoles(user._id, [ user.profile.role ] )
    )
    _.times(10,() -> Factory.create('fakeUser'))
    _.times(2,() -> Factory.create('fakeUser',{'profile.role': 'manager'}))

    Factory.define('fakeVolunteersForm',Volunteers.Collections.VolunteerForm,{
      'userId': () -> Factory.get('fakeUser'),
    })
    _.times(10,() -> Factory.create('fakeVolunteersForm'))

    getRandom = (name) -> _.sample(Factory.get(name).collection.find().fetch())

    Factory.define('fakeDivision',Volunteers.Collections.Division,
      {
        'name': () -> faker.company.companyName(),
        'parentId': () -> "TopEntity",
        'policy': () -> 'public',
        'description': () -> faker.lorem.paragraph(),
        'tags': () -> faker.lorem.words(),
      })
    _.times(10,() ->
      doc = Factory.create('fakeDivision')
      Roles.createRole(doc._id)
    )

    Factory.define('fakeDivisionLead',Volunteers.Collections.Lead,
      {
        'parentId': () -> getRandom('fakeDivision')._id,
        'userId': () -> getRandom('fakeUser')._id,
        'role': () -> 'lead',
        'title': () -> "Meta-Lead",
        'description': () -> faker.lorem.paragraph(),
        'position': () -> 'division',
        'policy': () -> _.sample(["public","requireApproval","adminOnly"]),
      })
    _.times(10,() -> Factory.create('fakeDivisionLead'))

    Factory.define('fakeDepartment',Volunteers.Collections.Department,
      {
        'name': () -> faker.company.companyName(),
        'policy': () -> 'public',
        'description': () -> faker.lorem.paragraph(),
        'tags': () -> faker.lorem.words(),
        'parentId': () -> getRandom('fakeDivision')._id
      })
    _.times(20,() ->
      doc = Factory.create('fakeDepartment')
      Roles.createRole(doc._id)
      Roles.addRolesToParent(doc._id, doc.parentId)
    )

    Factory.define('fakeDepartmentLead',Volunteers.Collections.Lead,
      {
        'parentId': () -> getRandom('fakeDepartment')._id,
        'userId': () -> getRandom('fakeUser')._id,
        'role': () -> 'lead',
        'title': () -> "2nd level Lead",
        'description': () -> faker.lorem.paragraph(),
        'position': () -> 'department',
        'policy': () -> _.sample(["public","requireApproval","adminOnly"]),
      })
    _.times(10,() ->
      doc = Factory.create('fakeDepartmentLead')
    )

    Factory.define('fakeTeam',Volunteers.Collections.Team,
      {
        'name': () -> faker.company.companyName(),
        # 'policy': () -> _.sample(["public","requireApproval","adminOnly"]),
        'policy': () -> 'public',
        'description': () -> faker.lorem.paragraph(),
        'tags': () -> faker.lorem.words(),
        'parentId': () -> getRandom('fakeDepartment')._id
      })
    _.times(40,() ->
      doc = Factory.create('fakeTeam')
      Roles.createRole(doc._id)
      Roles.addRolesToParent(doc._id, doc.parentId)
    )

    Factory.define('fakeTeamLead',Volunteers.Collections.Lead,
      {
        'parentId': () -> getRandom('fakeTeam')._id,
        'userId': () -> getRandom('fakeUser')._id,
        'role': () -> 'lead',
        'title': () -> "Head Chef",
        'description': () -> faker.lorem.paragraph(),
        'position': () -> 'team',
        'policy': () -> _.sample(["public","requireApproval","adminOnly"]),
      })
    _.times(15,() -> Factory.create('fakeTeamLead'))

    Factory.define('fakeTeamShifts',Volunteers.Collections.TeamShifts,
      {
        'parentId': () -> getRandom('fakeTeam')._id,
        'title': () -> faker.lorem.sentence(),
        'description': () -> faker.lorem.paragraph(),
        'policy': () -> _.sample(["public","requireApproval","adminOnly"]),
        'min': () -> faker.random.number(1,3),
        'max': () -> faker.random.number(4,6),
        'start': () -> faker.date.recent(30),
        'end': () -> moment(this.start).add(3, 'h').toDate()
      })
    _.times(50,() -> Factory.create('fakeTeamShifts'))

    Factory.define('fakeTeamTasks',Volunteers.Collections.TeamTasks,
      {
        'parentId': () -> getRandom('fakeTeam')._id,
        'title': () -> faker.lorem.sentence(),
        'description': () -> faker.lorem.paragraph(),
        'policy': () -> _.sample(["public","requireApproval","adminOnly"]),
        'dueDate': () -> faker.date.future(),
        'estimatedTime': () -> _.sample(["1-3hs", "3-6hs", "6-12hs","1d","2ds","more"]),
        'status': () -> _.sample(["pending", "archived", "done"])
        'min': () -> faker.random.number(1,3),
        'max': () -> faker.random.number(4,6),
      })
    _.times(50,() -> Factory.create('fakeTeamTasks'))
